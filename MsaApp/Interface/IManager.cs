﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MsaApp.Interface
{
    public interface IManager
    {
        bool Add(List<object> team);

        List<object> Get(int id);

        List<List<object>> GetAll();

        bool Update(int teamId, List<object> team);

        bool Delete(int teamId);

        int Count();
    }
}
