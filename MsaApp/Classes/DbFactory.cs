﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;
using System.Windows;

namespace MsaApp.Classes
{
    class DbFactory
    {
        private static MySqlConnection connection;
        private static string SERVER = "localhost";
        private static string UID = "root";
        private static string PASSWORD = "";
        private static string DATABASE = "msa";

        public static MySqlConnection GetConnection()
        {
            string connectionString = string.Format("SERVER={0};DATABASE={1};UID={2};PASSWORD={3}",
                                                    SERVER, DATABASE, UID, PASSWORD);
            connection = new MySqlConnection(connectionString);
            return connection;
        }

        public static bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:
                        MessageBox.Show("Cannot connect to server. Contact administrator");
                        break;
                    case 1045:
                        MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }

        public static bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        
    }
}
