﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using MahApps.Metro;
using MahApps.Metro.Controls;
using MySql.Data.MySqlClient;


namespace MsaApp.Views
{
    /// <summary>
    /// Interaction logic for SettingView.xaml
    /// </summary>
    public partial class SettingView : MetroWindow
    {
        private Views.NewTeamView newTeam;
        public int TrmId { get; set; }
        private MySqlConnection connection;
        private Models.Tournament TournamentManager;

        public SettingView()
        {
            InitializeComponent();

            connection = Classes.DbFactory.GetConnection();
            TournamentManager = new Models.Tournament(connection);
        }

        public SettingView(int trmId): this()
        {
            TrmId = trmId;

            List<object> trm = TournamentManager.Get(TrmId);
            string trmName = "";

            if (trm != null)
            {
                trmName = trm.ElementAt(1).ToString();
            }
            TrmLabel.Text = string.Format("Tournoi: {0}", trmName);
        }

        private void SettingApp_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void SettingApp_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            newTeam = null;
            this.Hide();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            newTeam = new Views.NewTeamView(TrmId, 1);
            newTeam.ShowDialog();
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            newTeam = new Views.NewTeamView(TrmId, 2);
            newTeam.ShowDialog();
        }
    }
}
