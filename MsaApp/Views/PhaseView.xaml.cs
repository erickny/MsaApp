﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using MySql.Data.MySqlClient;

namespace MsaApp.Views
{
    /// <summary>
    /// Interaction logic for PhaseView.xaml
    /// </summary>
    public partial class PhaseView : Window
    {
        private MySqlConnection connection;
        private Models.Team TeamManager;
        private Models.Tournament TournamentManager;
        private Models.Match MatchManager;
        private Models.Group GroupManager;

        public int TrmId { get; set; }

        public PhaseView()
        {
            InitializeComponent();

            connection = Classes.DbFactory.GetConnection();
            MatchManager = new Models.Match(connection);
            TournamentManager = new Models.Tournament(connection);
            TeamManager = new Models.Team(connection);
        }

        public PhaseView(int trmId): this()
        {
            TrmId = trmId;

            if (TournamentManager.IsSelected(TrmId))
            {
                LoadMatch();
            }
        }

        private void LoadMatch()
        {
            // Group A
            List<List<object>> listGroupA = MatchManager.Get(TrmId, 1, 1);

            List<object> teamA11 = TeamManager.Get(int.Parse(listGroupA.ElementAt(0).ElementAt(3).ToString()));
            List<object> teamA12 = TeamManager.Get(int.Parse(listGroupA.ElementAt(0).ElementAt(4).ToString()));
            List<object> teamA21 = TeamManager.Get(int.Parse(listGroupA.ElementAt(1).ElementAt(3).ToString()));
            List<object> teamA22 = TeamManager.Get(int.Parse(listGroupA.ElementAt(1).ElementAt(4).ToString()));
            List<object> teamA31 = TeamManager.Get(int.Parse(listGroupA.ElementAt(2).ElementAt(3).ToString()));
            List<object> teamA32 = TeamManager.Get(int.Parse(listGroupA.ElementAt(2).ElementAt(4).ToString()));

            Team1GroupAMatch1.Text = teamA11.ElementAt(0).ToString();
            Team2GroupAMatch1.Text = teamA12.ElementAt(0).ToString();
            Team1GroupAMatch2.Text = teamA21.ElementAt(0).ToString();
            Team3GroupAMatch2.Text = teamA22.ElementAt(0).ToString();            
            Team2GroupAMatch3.Text = teamA31.ElementAt(0).ToString();
            Team3GroupAMatch3.Text = teamA32.ElementAt(0).ToString();
            
            // Group B
            List<List<object>> listGroupB = MatchManager.Get(TrmId, 1, 2);

            List<object> teamB11 = TeamManager.Get(int.Parse(listGroupB.ElementAt(0).ElementAt(3).ToString()));
            List<object> teamB12 = TeamManager.Get(int.Parse(listGroupB.ElementAt(0).ElementAt(4).ToString()));
            List<object> teamB21 = TeamManager.Get(int.Parse(listGroupB.ElementAt(1).ElementAt(3).ToString()));
            List<object> teamB22 = TeamManager.Get(int.Parse(listGroupB.ElementAt(1).ElementAt(4).ToString()));
            List<object> teamB31 = TeamManager.Get(int.Parse(listGroupB.ElementAt(2).ElementAt(3).ToString()));
            List<object> teamB32 = TeamManager.Get(int.Parse(listGroupB.ElementAt(2).ElementAt(4).ToString()));

            Team1GroupBMatch1.Text = teamB11.ElementAt(0).ToString();
            Team2GroupBMatch1.Text = teamB12.ElementAt(0).ToString();
            Team1GroupBMatch2.Text = teamB21.ElementAt(0).ToString();
            Team3GroupBMatch2.Text = teamB22.ElementAt(0).ToString();            
            Team2GroupBMatch3.Text = teamB31.ElementAt(0).ToString();
            Team3GroupBMatch3.Text = teamB32.ElementAt(0).ToString();

            // Group C
            List<List<object>> listGroupC = MatchManager.Get(TrmId, 1, 3);

            List<object> teamC11 = TeamManager.Get(int.Parse(listGroupC.ElementAt(0).ElementAt(3).ToString()));
            List<object> teamC12 = TeamManager.Get(int.Parse(listGroupC.ElementAt(0).ElementAt(4).ToString()));
            List<object> teamC21 = TeamManager.Get(int.Parse(listGroupC.ElementAt(1).ElementAt(3).ToString()));
            List<object> teamC22 = TeamManager.Get(int.Parse(listGroupC.ElementAt(1).ElementAt(4).ToString()));
            List<object> teamC31 = TeamManager.Get(int.Parse(listGroupC.ElementAt(2).ElementAt(3).ToString()));
            List<object> teamC32 = TeamManager.Get(int.Parse(listGroupC.ElementAt(2).ElementAt(4).ToString()));

            Team1GroupCMatch1.Text = teamC11.ElementAt(0).ToString();
            Team2GroupCMatch1.Text = teamC12.ElementAt(0).ToString();
            Team1GroupCMatch2.Text = teamC21.ElementAt(0).ToString();
            Team3GroupCMatch2.Text = teamC22.ElementAt(0).ToString();
            Team2GroupCMatch3.Text = teamC31.ElementAt(0).ToString();
            Team3GroupCMatch3.Text = teamC32.ElementAt(0).ToString();

            // Group D
            List<List<object>> listGroupD = MatchManager.Get(TrmId, 1, 4);

            List<object> teamD11 = TeamManager.Get(int.Parse(listGroupD.ElementAt(0).ElementAt(3).ToString()));
            List<object> teamD12 = TeamManager.Get(int.Parse(listGroupD.ElementAt(0).ElementAt(4).ToString()));
            List<object> teamD21 = TeamManager.Get(int.Parse(listGroupD.ElementAt(1).ElementAt(3).ToString()));
            List<object> teamD22 = TeamManager.Get(int.Parse(listGroupD.ElementAt(1).ElementAt(4).ToString()));
            List<object> teamD31 = TeamManager.Get(int.Parse(listGroupD.ElementAt(2).ElementAt(3).ToString()));
            List<object> teamD32 = TeamManager.Get(int.Parse(listGroupD.ElementAt(2).ElementAt(4).ToString()));

            Team1GroupDMatch1.Text = teamD11.ElementAt(0).ToString();
            Team2GroupDMatch1.Text = teamD12.ElementAt(0).ToString();
            Team1GroupDMatch2.Text = teamD21.ElementAt(0).ToString();
            Team3GroupDMatch2.Text = teamD22.ElementAt(0).ToString();
            Team2GroupDMatch3.Text = teamD31.ElementAt(0).ToString();
            Team3GroupDMatch3.Text = teamD32.ElementAt(0).ToString();
        }

        private void Phase_KeyDown(object sender, KeyEventArgs e)
        {
            switch(e.Key)
            {
                case Key.Escape: ExitFullScreen();
                    break;
                case Key.F11: SetFullScreen();
                    break;
                case Key.Delete: QuitApplication();
                    break;
            }
        }

        private void ExitFullScreen()
        {
            if (this.WindowStyle.Equals(WindowStyle.None))
                this.WindowStyle = WindowStyle.SingleBorderWindow;
        }

        private void SetFullScreen()
        {
            if (!this.WindowStyle.Equals(WindowStyle.None))
            {
                this.WindowState = WindowState.Normal;
                this.WindowStyle = WindowStyle.None;
                this.WindowState = WindowState.Maximized;
            }
        }

        private void QuitApplication()
        {
            this.Hide();
        }
    }
}
