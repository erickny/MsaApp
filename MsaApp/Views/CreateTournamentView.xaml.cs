﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using MahApps.Metro;
using MahApps.Metro.Controls;

using MySql.Data.MySqlClient;

namespace MsaApp.Views
{
    /// <summary>
    /// Interaction logic for CreateTournamentView.xaml
    /// </summary>
    public partial class CreateTournamentView : MetroWindow
    {
        private MySqlConnection connection;
        private Models.Tournament TournamentManager;

        public CreateTournamentView()
        {
            InitializeComponent();

            connection = Classes.DbFactory.GetConnection();
            TournamentManager = new Models.Tournament(connection);
        }

        private void CreateTournamentApp_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void CreateTournamentApp_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            HideWindow();
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            string titleTrm = TitleTrm.Text.Trim();
            string dateTrm = ChangeDateFormat(DateTrm.Text);

            if (!string.IsNullOrEmpty(titleTrm) && !string.IsNullOrEmpty(dateTrm))
            {
                List<object> tournament = new List<object>();
                tournament.Add(titleTrm);
                tournament.Add(dateTrm);

                if (TournamentManager.Add(tournament))
                {
                    MessageBox.Show("OK");
                    HideWindow();
                }
                else
                {
                    MessageBox.Show("Error");
                }
            }
        }

        private string ChangeDateFormat(string oldDate)
        {
            // 31/12/9999 into 9999-12-31

            string[] dateArray = oldDate.Split('/');
            string newFormattedDate = string.Format("{0}-{1}-{2}", dateArray[2], dateArray[1], dateArray[0]);

            return newFormattedDate;
        }

        private void HideWindow()
        {
            this.Hide();
        }
    }
}
