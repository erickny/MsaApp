﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using MahApps.Metro;
using MahApps.Metro.Controls;
using MySql.Data.MySqlClient;

namespace MsaApp.Views
{
    /// <summary>
    /// Interaction logic for NewTeamView.xaml
    /// </summary>
    public partial class NewTeamView : MetroWindow
    {
        private Models.Team TeamManager;
        private MySqlConnection connection;

        public int TrmId { get; set; }
        public int TeamId { get; set; }

        public NewTeamView()
        {
            InitializeComponent();

            connection = Classes.DbFactory.GetConnection();
            TeamManager = new Models.Team(connection);
        }

        public NewTeamView(int trmId, int optionId): this()
        {
            TrmId = trmId;

            if (optionId == 1)
            {
                this.Title = "Ajouter";
            }
            if (optionId == 2)
            {
                this.Title = "Modifier";
                Logo.Visibility = Visibility.Hidden;
                LogoStack.Visibility = Visibility.Hidden;
            }
        }

        public NewTeamView(int trmId, int optionId, int teamId) : this(trmId, optionId)
        {
            TeamId = teamId;
        }

        private void NewTeam_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void NewTeam_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            HideWindow();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            HideWindow();
        }

        private void btnValid_Click(object sender, RoutedEventArgs e)
        {
            string teamName = TeamName.Text.Trim();
            string logo = "default.png";
            int number = 0;
            int id_groupe = 5;
            int id_tournament = TrmId;

            if (!string.IsNullOrEmpty(teamName))
            {
                // name, logo, numero, date_add, id_groupe, id_tournament
                List<object> team = new List<object>();
                team.Add(teamName);
                team.Add(logo);
                team.Add(number);
                team.Add(id_groupe);
                team.Add(id_tournament);

                if (TeamManager.Add(team))
                {
                    MessageBox.Show("OK");
                    TeamName.Text = "";
                    TeamName.Focus();
                    TeamLogo.Text = "";
                }
                else
                { 
                    MessageBox.Show("Erreur"); 
                }
            }
        }

        private void HideWindow()
        {
            this.Hide();
        }

        private void btnUpload_Click(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
