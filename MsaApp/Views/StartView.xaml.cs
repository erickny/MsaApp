﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using MahApps.Metro;
using MahApps.Metro.Controls;

using MySql.Data.MySqlClient;

namespace MsaApp.Views
{
    /// <summary>
    /// Interaction logic for StatView.xaml
    /// </summary>
    public partial class StatView : MetroWindow
    {
        private MainWindow mainWindow;
        private Models.Tournament TournamentManager;
        private MySqlConnection connection;
        public int TrmId { get; set; }

        public StatView()
        {
            InitializeComponent();

            connection = Classes.DbFactory.GetConnection();
            TournamentManager = new Models.Tournament(connection);

            InitComboList();
        }

        private void StartApp_KeyDown(object sender, KeyEventArgs e)
        {
            switch(e.Key)
            {
                case Key.E: ShowMainWindow();
                    break;
            }
        }

        public void InitComboList()
        {
            List<List<object>> listTournament = TournamentManager.GetAll();
            ComboBoxItem item;

            foreach(List<object> trm in listTournament)
            {
                item = new ComboBoxItem();
                item.Name = "item" + trm.ElementAt(0);
                item.Content = trm.ElementAt(1);
                TournamentComboList.Items.Add(item);
            }
        }

        private void ShowMainWindow()
        {
            if (TrmId > 0)
            {
                mainWindow = new MainWindow(TrmId);
                mainWindow.Show();
                this.Hide();
            }            
        }

        private void TournamentComboList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxItem selectedComboBoxItem = (ComboBoxItem)TournamentComboList.SelectedItem;
            if (selectedComboBoxItem != null)
            {
                string selectedItem = selectedComboBoxItem.Content.ToString();
                TrmId = int.Parse(selectedComboBoxItem.Name.Substring(4));
                SelectedTournamentLabel.Text = string.Format("{0}", selectedItem);
            }
            else SelectedTournamentLabel.Text = "";
        }

        private void AcceptButton_Click(object sender, RoutedEventArgs e)
        {
            ShowMainWindow();
        }

        private void CreateButton_Click(object sender, RoutedEventArgs e)
        {
            CreateTournamentView createTournamentView = new CreateTournamentView();
            createTournamentView.ShowDialog();
        }

        private void StartApp_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Environment.Exit(0);
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            TournamentComboList.Items.Clear();

            connection = Classes.DbFactory.GetConnection();
            TournamentManager = new Models.Tournament(connection);

            List<List<object>> listTournament = TournamentManager.GetAll();
            ComboBoxItem item;

            foreach (List<object> trm in listTournament)
            {
                item = new ComboBoxItem();
                item.Name = "item" + trm.ElementAt(0);
                item.Content = trm.ElementAt(1);
                TournamentComboList.Items.Add(item);
            }
        }

    }
}
