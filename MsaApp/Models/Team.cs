﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;

namespace MsaApp.Models
{
    class Team: Interface.IManager
    {
        private MySqlConnection connection;

        public Team(MySqlConnection connection)
        {
            this.connection = connection;
        }
          
        public bool Add(List<object> team)
        {
            // name, logo, numero, date_add, id_groupe, id_tournament
            string query = "INSERT INTO teams(name, logo, numero, date_add, id_groupe, id_tournament) " +
                            "VALUES(@name, @logo, @numero, NOW(), @id_groupe, @id_tournament)";
           
            if (Classes.DbFactory.OpenConnection())
            {
                MySqlCommand command = new MySqlCommand(query, connection);
                command.Parameters.AddWithValue("name", team.ElementAt(0));
                command.Parameters.AddWithValue("logo", team.ElementAt(1));
                command.Parameters.AddWithValue("numero", team.ElementAt(2));
                command.Parameters.AddWithValue("id_groupe", team.ElementAt(3));
                command.Parameters.AddWithValue("id_tournament", team.ElementAt(4));

                command.ExecuteNonQuery();
                Classes.DbFactory.CloseConnection();
                return true;
            }
            return false;
        }

        public List<object> Get(int id)
        {
            List<object> team = new List<object>();
            string query = "SELECT name, logo, numero FROM teams WHERE id = @id";

            if (Classes.DbFactory.OpenConnection())
            {
                MySqlCommand command = new MySqlCommand(query, connection);
                command.Parameters.AddWithValue("id", id);
                MySqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();
                    team.Add(reader["name"]);
                    team.Add(reader["logo"]);
                }
                reader.Close();
                Classes.DbFactory.CloseConnection();
                return team;
            }
            return null;
        }
        
        public List<List<object>> GetFromIdTrm(int idTrm)
        {
            List<List<object>> listTeams = new List<List<object>>();
            string query = "SELECT id, name, logo, numero, date_add, id_groupe, id_tournament FROM teams WHERE id_tournament = @id ORDER BY id LIMIT 12";

            if (Classes.DbFactory.OpenConnection())
            {
                MySqlCommand command = new MySqlCommand(query, connection);
                command.Parameters.AddWithValue("id", idTrm);
                MySqlDataReader reader = command.ExecuteReader();

                List<object> item;
                while (reader.Read())
                {
                    item = new List<object>();
                    item.Add(reader["id"]);
                    item.Add(reader["name"]);
                    item.Add(reader["logo"]);
                    item.Add(reader["numero"]);
                    item.Add(reader["date_add"]);
                    item.Add(reader["id_groupe"]);
                    item.Add(reader["id_tournament"]);
                    listTeams.Add(item);
                }
                reader.Close();
                Classes.DbFactory.CloseConnection();
                return listTeams;
            }
            return null;
        }

        public List<List<object>> GetTeamsFromIdTrm(int idTrm)
        {
            List<List<object>> listTeams = new List<List<object>>();
            string query = "SELECT id, name, logo, numero, date_add, id_groupe, id_tournament FROM teams WHERE id_tournament = @id ORDER BY id_groupe LIMIT 12";

            if (Classes.DbFactory.OpenConnection())
            {
                MySqlCommand command = new MySqlCommand(query, connection);
                command.Parameters.AddWithValue("id", idTrm);
                MySqlDataReader reader = command.ExecuteReader();

                List<object> item;
                while (reader.Read())
                {
                    item = new List<object>();
                    item.Add(reader["id"]);
                    item.Add(reader["name"]);
                    item.Add(reader["logo"]);
                    item.Add(reader["numero"]);
                    item.Add(reader["date_add"]);
                    item.Add(reader["id_groupe"]);
                    item.Add(reader["id_tournament"]);
                    listTeams.Add(item);
                }
                reader.Close();
                Classes.DbFactory.CloseConnection();
                return listTeams;
            }
            return null;
        }

        public List<List<object>> GetAll()
        {
            throw new NotImplementedException();
        }

        public bool Update(int teamId, List<object> team)
        {
            return false;
        }

        public bool UpdateNumGroup(int teamId, int num, int group)
        {
            string query = "UPDATE teams SET numero=@numero, id_groupe=@id_group WHERE id = @id";

            if (Classes.DbFactory.OpenConnection())
            {
                MySqlCommand command = new MySqlCommand(query, connection);
                command.Parameters.AddWithValue("id", teamId);
                command.Parameters.AddWithValue("numero", num);
                command.Parameters.AddWithValue("id_group", group);
                command.ExecuteNonQuery();
                Classes.DbFactory.CloseConnection();
                return true;
            }
            return false;
        }
        
        public bool Delete(int teamId)
        {
            throw new NotImplementedException();
        }

        public int Count()
        {
            throw new NotImplementedException();
        }
    }
}
