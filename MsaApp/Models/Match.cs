﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;

namespace MsaApp.Models
{
    class Match: Interface.IManager
    {
        // id	label	date_match	score_first_team	
        // score_second_team	id_first_team	id_second_team	id_tournament	id_step
        private MySqlConnection connection;

        public Match(MySqlConnection connection)
        {
            this.connection = connection;
        }

        public bool Add(List<object> match)
        {
            string query = "INSERT INTO matchs(label, date_match, score_first_team, score_second_team, id_first_team, id_second_team, id_tournament, id_step, id_group) " +
                            "VALUES(@label, @date_match, @score_first_team, @score_second_team, @id_first_team, @id_second_team, @id_tournament, @id_step, @id_group)";

            if (Classes.DbFactory.OpenConnection())
            {
                MySqlCommand command = new MySqlCommand(query, connection);
                command.Parameters.AddWithValue("label", match.ElementAt(0));
                command.Parameters.AddWithValue("date_match", match.ElementAt(1));
                command.Parameters.AddWithValue("score_first_team", match.ElementAt(2));
                command.Parameters.AddWithValue("score_second_team", match.ElementAt(3));
                command.Parameters.AddWithValue("id_first_team", match.ElementAt(4));
                command.Parameters.AddWithValue("id_second_team", match.ElementAt(5));
                command.Parameters.AddWithValue("id_tournament", match.ElementAt(6));
                command.Parameters.AddWithValue("id_step", match.ElementAt(7));
                command.Parameters.AddWithValue("id_group", match.ElementAt(8));

                command.ExecuteNonQuery();
                Classes.DbFactory.CloseConnection();
                return true;
            }
            return false;
        }

        public List<List<object>> Get(int trmId, int stepId, int groupId)
        {
            List<List<object>> listMatch = new List<List<object>>();
            string query = "SELECT matchs.id, matchs.date_match, matchs.score_first_team, matchs.score_second_team, "+
                            "matchs.id_first_team, matchs.id_second_team, groups.name AS group_name, steps.name as step_name "+
                            "FROM matchs, tournaments, groups, steps "+
                            "WHERE matchs.id_tournament=tournaments.id AND matchs.id_group=groups.id AND matchs.id_step=steps.id AND "+
                            "matchs.id_tournament=@id_tournament AND matchs.id_step=@id_step AND matchs.id_group=@id_group";
            
            if (Classes.DbFactory.OpenConnection())
            {
                MySqlCommand command = new MySqlCommand(query, connection);
                command.Parameters.AddWithValue("id_tournament", trmId);
                command.Parameters.AddWithValue("id_step", stepId);
                command.Parameters.AddWithValue("id_group", groupId);

                MySqlDataReader reader = command.ExecuteReader();
                List<object> item;

                while (reader.Read())
                {                    
                    item = new List<object>();
                    item.Add(reader["id"]);
                    item.Add(reader["score_first_team"]);
                    item.Add(reader["score_second_team"]);
                    item.Add(reader["id_first_team"]);
                    item.Add(reader["id_second_team"]);
                    item.Add(reader["group_name"]);
                    item.Add(reader["step_name"]);
                    listMatch.Add(item);
                }

                reader.Close();
                Classes.DbFactory.CloseConnection();
                return listMatch;
            }
            return null;            
        }

        public List<object> Get(int id)
        {
            throw new NotImplementedException();
        }

        public List<List<object>> GetAll()
        {
            throw new NotImplementedException();
        }

        public bool Update(int teamId, List<object> team)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int teamId)
        {
            throw new NotImplementedException();
        }

        public int Count()
        {
            throw new NotImplementedException();
        }
                
    }
}
